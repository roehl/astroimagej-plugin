package CASObservatory;

import java.awt.Button;
import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Scrollbar;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import CASObservatory.api.CASFitsReader;
import ij.IJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.PlugIn;
import ij.process.ImageProcessor;

public class Spectrum_1D_to_2D implements PlugIn {

	TextField textWidth;
	Label labelFile;
	int imageHigh = 20;
	String filename;
	String dirname;
	CASFitsReader reader;
	double data[] = null;

	public void run(String arg) {

		dlg();

	}

	private Panel makePanel(GenericDialog gd) {
		Panel panel = new Panel();

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 30, 30, 30, 40, 100 };
		gridBagLayout.rowHeights = new int[] { 0, 30, 30, 30, 30 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0 };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0 };
		panel.setLayout(gridBagLayout);

		Label labelText = new Label("Creates a new 2D-Image form a given 1D-Spectrum");
		GridBagConstraints gbc_labelText = new GridBagConstraints();
		gbc_labelText.gridwidth = 5;
		gbc_labelText.insets = new Insets(0, 0, 5, 0);
		gbc_labelText.gridx = 0;
		gbc_labelText.gridy = 0;
		panel.add(labelText, gbc_labelText);

		Button btnChoose = new Button("Choose 1D Spectrum...");
		btnChoose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (openFile()) {
					labelFile.setText("1D-Spectrum: " + filename);
					// System.out.println("Dir:" + dirname);
					// System.out.println("File:" + filename);
					reader = new CASFitsReader(dirname,filename);
					data = reader.readSpectrum();
				}
			}
		});
		GridBagConstraints gbc_btnChoose = new GridBagConstraints();
		gbc_btnChoose.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnChoose.gridwidth = 5;
		gbc_btnChoose.insets = new Insets(0, 0, 5, 0);
		gbc_btnChoose.gridx = 0;
		gbc_btnChoose.gridy = 1;
		panel.add(btnChoose, gbc_btnChoose);

		labelFile = new Label("File:                                                                               ");

		GridBagConstraints gbc_labelFile = new GridBagConstraints();
		gbc_labelFile.anchor = GridBagConstraints.WEST;
		gbc_labelFile.gridwidth = 5;
		gbc_labelFile.insets = new Insets(0, 0, 5, 0);
		gbc_labelFile.gridx = 0;
		gbc_labelFile.gridy = 2;
		panel.add(labelFile, gbc_labelFile);

		Label label_1 = new Label("Image High");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.gridx = 0;
		gbc_label_1.gridy = 4;
		panel.add(label_1, gbc_label_1);

		Scrollbar scrollbar = new Scrollbar();

		scrollbar.addAdjustmentListener(new AdjustmentListener() {
			public void adjustmentValueChanged(AdjustmentEvent e) {

				imageHigh = scrollbar.getValue();
				textWidth.setText("" + imageHigh);

			}
		});
		scrollbar.setValue(imageHigh);
		textWidth = new TextField();
		textWidth.setEditable(false);
		textWidth.setText("   20   ");
		GridBagConstraints gbc_textWidth = new GridBagConstraints();
		gbc_textWidth.gridwidth = 2;
		gbc_textWidth.insets = new Insets(0, 0, 5, 5);
		gbc_textWidth.gridx = 1;
		gbc_textWidth.gridy = 4;
		panel.add(textWidth, gbc_textWidth);

		scrollbar.setBlockIncrement(1);
		scrollbar.setMaximum(510);
		scrollbar.setMinimum(1);
		scrollbar.setOrientation(Scrollbar.HORIZONTAL);
		GridBagConstraints gbc_scrollbar = new GridBagConstraints();
		gbc_scrollbar.fill = GridBagConstraints.HORIZONTAL;
		gbc_scrollbar.gridwidth = 2;
		gbc_scrollbar.insets = new Insets(0, 0, 5, 0);
		gbc_scrollbar.gridx = 3;
		gbc_scrollbar.gridy = 4;
		panel.add(scrollbar, gbc_scrollbar);

		return panel;

	}

	boolean dlg() {
		GenericDialog gd = new GenericDialog("1D-FITS-Spectrum \u21B7 2D-Image");

		gd.addPanel(makePanel(gd));
		gd.showDialog();		
		
		if (gd.wasCanceled())
			return false;
		
		// ok was pressed!
		createImage();

		return true;

	}

	private boolean openFile() {

		Frame parent = IJ.getInstance();

		FileDialog fd = new FileDialog(parent, "Choose a file", FileDialog.LOAD);
		String home = System.getProperty("user.home");
		fd.setDirectory(home);
		fd.setFile("*.fits");
		fd.setVisible(true);

		filename = fd.getFile();

		// cancelled ?
		if (filename == null)
			return false;

		dirname = fd.getDirectory();

		return true;
	}

	public boolean createImage() {

		String title = "2D Spectrum Image - from " + filename;

		int imageWidth =  data.length;
		double profile[] = data;
		ImagePlus myIMP = IJ.createImage(title, "GRAY32", imageWidth, imageHigh, 1);
		ImageProcessor ip = myIMP.getProcessor();

		for (int i = 0; i < imageWidth; i++) {
			for (int j = 0; j < imageHigh; j++) {
				ip.putPixelValue(i, j, profile[i]);
			}
		}

		myIMP.show();
		IJ.selectWindow(title);
		IJ.run("Enhance Contrast", "saturated=0.5");
		return true;
	}
	
}
