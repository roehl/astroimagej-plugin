package CASObservatory;

import CASObservatory.core.CASPlot;
import CASObservatory.core.CASPlotWindow;
import CASObservatory.core.CASProfilePlot;
import ij.IJ;
import ij.ImagePlus;
import ij.Prefs;
import ij.gui.GenericDialog;
import ij.gui.ImageCanvas;
import ij.gui.Plot;
import ij.gui.PlotMaker;
import ij.gui.Roi;
import ij.plugin.PlugIn;

/**
 * Implements the Analyze/Plot Profile and Edit/Options/Profile Plot Options
 * commands.
 */
public class Spectrum_2D_to_1D implements PlugIn, PlotMaker {
	ImagePlus imp;
	boolean firstTime = true;
	boolean plotVertically;
	ImageCanvas canvas;
	CASPlotWindow cpw;
	CASProfilePlot cpp;
	double kSigma = 1.0;

	public void run(String arg) {

		imp = IJ.getImage();
		if (firstTime)
			plotVertically = Prefs.verticalProfile || IJ.altKeyDown();
		CASPlot casplot = getPlot(kSigma);

		firstTime = false;
		if (casplot == null)
			return;

		casplot.setPlotMaker(this);
		cpw = casplot.show();

		// NonBlockingGenericDialog dlg = new NonBlockingGenericDialog("dlg");
		// dlg.addMessage("--------glg--------");
		// dlg.showDialog();

	}

	public void updatePlot() {

		// FIXME live button muss zuerst gedrückt werden!
		CASPlot update = getPlot(kSigma);
		cpw.drawPlot(update);

	}

	boolean getParameters() {
		GenericDialog dlg = new GenericDialog("1D Spectrum Profile", IJ.getInstance());
		dlg.addNumericField("k =", 1, 2);
		dlg.showDialog();
		if (!dlg.wasCanceled()) {
			kSigma = dlg.getNextNumber();
			return true;
		} else
			return false;
	}

	@Override
	public Plot getPlot() {
		return getPlot(kSigma);
	}

	public CASPlot getPlot(double kSigma) {
		Roi roi = imp.getRoi();
		if (roi == null || !(roi.isLine() || roi.getType() == Roi.RECTANGLE)) {
			if (firstTime)
				IJ.error("CASPlot Profile", "Line or rectangular selection required");
			return null;
		}

		if (cpp == null)
			cpp = new CASProfilePlot(this, imp, plotVertically);

		cpp.calculate(CASPlotWindow.getAverageMethod(), kSigma);

		return cpp.getPlot();
	}

	public ImagePlus getSourceImage() {
		return imp;
	}

}
