package CASObservatory.api;

import java.awt.Rectangle;
import java.io.FileWriter;
import java.io.IOException;

public class CASFile {

	public void exportMidas(String dir, String file, double[] data, Rectangle rect, String fileOrigin, int method) {
		CASFitsWriter fitfile = new CASFitsWriter();
		String path = dir + file;
		fitfile.save(path, data, rect, fileOrigin,method);
	}

	public boolean exportASCII(String dir, String file, double[] data, String sep, String header) {
		String path = dir + file;
		FileWriter f1;

		try {
			f1 = new FileWriter(path, false /* append */);
			f1.write(header + "\n");
			for (int i = 0; i < data.length; i++) {
				String line = i + sep + data[i];
				if (i < data.length - 1)
					line += "\n";
				f1.write(line);
			}
			f1.close();
		} catch (IOException e) {
			System.out.println("ERROR: class: SOCLog method: writer index: 0");
			return false;
		}

		return true;

	}

	public void exportVOTABLE(String dir, String file) {
		// reserved for future used
	}
}
