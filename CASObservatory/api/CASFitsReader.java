package CASObservatory.api;

import java.io.IOException;

import ij.IJ;
import nom.tam.fits.Fits;
import nom.tam.fits.FitsException;
import nom.tam.fits.ImageHDU;
import nom.tam.image.StandardImageTiler;

/**
 * using the nom.tam FITS library
 *
 */
public class CASFitsReader {

	Fits f;

	public CASFitsReader(String directory, String fileName) {

		String path = directory + fileName;
		try {
			f = new Fits(path);

		} catch (FitsException e) {
			IJ.error("This does not appear to be a FITS file.");
			IJ.showStatus("");
			// e.printStackTrace();
		}
	}

	public double[] readSpectrum() {

		float[] tmp = null;
		try {
			ImageHDU imageHDU = (ImageHDU) f.readHDU();
			StandardImageTiler tiler = imageHDU.getTiler();
			tmp = (float[]) tiler.getCompleteImage();

		} catch (FitsException | IOException e) {

			e.printStackTrace();
		}

		// convert
		double[] res = new double[tmp.length];
		for (int i = 0; i < res.length; i++) {
			res[i] = tmp[i];
		}

		return res;

	}

	@SuppressWarnings("unused")
	private void plot(double[] liste) {

		for (int i = 0; i < liste.length; i++) {
			System.out.println(i + "\t" + liste[i]);
		}
	}
}
