package CASObservatory.api;

import java.awt.Rectangle;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ij.IJ;

/**
 * The Source is mainly based on FITS_Writer.java from ImageJ. Developer K.A.
 * Colling and F.V. Hessman.
 */
public class CASFitsWriter {

	String path;
	double profile[];
	int len = 0;
	Rectangle rect;
	String originFile;
	String strMethod = "median";

	/**
	 * save Create an uncalibrated 1d-spectrum in FITSFormat. The file is
	 * verified by the NASA FITS File Verifier and can be read from
	 * midas-software.
	 * 
	 * @param path
	 *            Director & Filename to store the spectrum
	 * @param profile
	 *            List of data values (y-axis - because the x-axis are pixel
	 *            numbers)
	 * @param rect
	 *            The rectangle region form the original image source
	 * @param originFile
	 *            Filename of the source image
	 * @param method
	 *            The method used to create the spectrum
	 */
	public void save(String path, double[] profile, Rectangle rect, String originFile, int method) {

		File file = new File(path);
		file.delete();

		this.path = path;
		this.profile = profile;
		this.rect = rect;
		this.originFile = originFile;

		len = profile.length;
		// method == 0 default (median)
		if (method == 1)
			strMethod = "sigma-median";
		if (method == 2)
			strMethod = "mean";

		/** Create the file */
		writeHeader();
		writeData();
		writeFooter();
	}

	private void writeHeader() {

		String empty = "\'                  \'";
		String strSource = "Image src: " + originFile;
		String strRect = "Image Region [" + rect.width + "," + rect.height + "] at(" + rect.x + "," + rect.y + ")";
		String strMethode = "Image Method: " + strMethod;
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
		String strDate = "Image Created: " + formatter.format(date);

		int numCards = 15; // MUST BE THE NUMBER OF wireCard calls!!!

		appendLine(writeCard("SIMPLE", "                   T", "ImageJ by CAS-Observatory"), path);
		appendLine(writeCard("BITPIX", "                 -32", "No. of bits per pixel"), path);
		appendLine(writeCard("NAXIS", "                   1", "No. of axes"), path);
		appendLine(writeCard("NAXIS1", naxis(len), "No. of pixels"), path);
		appendLine(writeCard("EXTEND", "                   T", "FITS extension may be present"), path);
		appendLine(writeCard("CRPIX1", "                  1.", "Reference pixel"), path);
		appendLine(writeCard("CRVAL1", "                  1.", "Coordinate at reference pixel"), path);
		appendLine(writeCard("CDELT1", "                  1.", "Coord. incr. per pixel (orginal value)"), path);
		appendLine(writeCard("CTYPE1", empty, "Units of coordinate"), path);
		appendLine(writeCard("BUNIT", empty, "Units of data values"), path);
		appendLine(writeCard("OBJECT", empty, "object"), path);
		appendLine(writeCard("HISTORY", empty, strSource), path);
		appendLine(writeCard("HISTORY", empty, strRect), path);
		appendLine(writeCard("HISTORY", empty, strDate), path);
		appendLine(writeCard("HISTORY", empty, strMethode), path);

		int fillerSize = 2880 - ((numCards * 80 + 3) % 2880);
		char[] end = new char[3];
		end[0] = 'E';
		end[1] = 'N';
		end[2] = 'D';

		char[] filler = new char[fillerSize];
		for (int i = 0; i < fillerSize; i++)
			filler[i] = ' ';
		appendLine(end, path);
		appendLine(filler, path);

	}

	private String naxis(int value) {
		int length = 20 - String.valueOf(value).length();
		String spaces = String.format("%" + length + "s", "");
		return spaces + value;
	}

	/**
	 * Writes one line of a FITS header
	 */
	private char[] writeCard(String title, String value, String comment) {
		char[] card = new char[80];
		for (int i = 0; i < 80; i++)
			card[i] = ' ';

		s2ch(title, card, 0);
		card[8] = '=';
		s2ch(value, card, 10);
		card[31] = '/';
		card[32] = ' ';
		s2ch(comment, card, 33);
		return card;
	}

	/**
	 * Converts a String to a char[]
	 */
	private void s2ch(String str, char[] ch, int offset) {
		int j = 0;
		for (int i = offset; i < 80 && i < str.length() + offset; i++)
			ch[i] = str.charAt(j++);
	}

	/**
	 * Appends 'line' to the end of the file specified by 'path'.
	 */
	private void appendLine(char[] line, String path) {
		try {
			FileWriter output = new FileWriter(path, true);
			output.write(line);
			output.close();
		} catch (IOException e) {
			IJ.showStatus("Error writing file!");
			return;
		}
	}

	private void writeFooter() {

		int fillerLength = 2880 - ((4 * len) % 2880);
		char[] endFiller = new char[fillerLength];
		appendLine(endFiller, path);

	}

	/**
	 * Appends the data of the current image to the end of the file specified by
	 * path.
	 */
	private void writeData() {

		try {
			DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(path, true)));
			for (int i = 0; i < profile.length; i++)
				dos.writeFloat((float) profile[i]);
			dos.close();
		} catch (IOException e) {
			IJ.showStatus("Error writing file!");
			return;
		}
	}

	// /**
	// * Converts a string into an 80-char array.
	// */
	// private char[] eighty(String s) {
	//
	// char[] c = new char[80];
	// int l = s.length();
	// for (int i = 0; i < l && i < 80; i++)
	// c[i] = s.charAt(i);
	// if (l < 80) {
	// for (; l < 80; l++)
	// c[l] = ' ';
	// }
	// return c;
	// }

}
