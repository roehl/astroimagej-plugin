package CASObservatory.api;

import java.util.Arrays;
import java.util.Vector;

/**
 * 
 * double values[] = { 1, 2, 3, 3, 3, 4, 5, 6, 7, 7, 30 }; CASStatistics stat =
 * new CASStatistics(); System.out.println("mean:\t" + stat.mean(values));
 * System.out.println("median:\t" + stat.median(values));
 * System.out.println("sigmamedian:\t" + stat.sigmaMedian(values, 1));
 * System.out.println("std:\t" + stat.std(values)); Results should be: mean =
 * 6.45.. , median = 4; sigma-median = 3.5, std = 8.05..
 */
public class CASStatistics {

	public double mean(double values[]) {
		int len = values.length;
		if (len < 1)
			return 0.0;

		double res = 0.0;

		for (int i = 0; i < len; i++)
			res += values[i];

		return res / (len * 1.0);
	}

	public double median(double values[]) {
		int len = values.length;
		if (len < 1)
			return 0.0;

		Arrays.sort(values);
		double res = values[len / 2];

		if (len % 2 == 0 /* even */)
			res = (values[len / 2 - 1] + values[len / 2]) / 2.0;

		return res;
	}

	public double sigmaMedian(double values[], double kSigma) {
		int len = values.length;
		if (len < 1)
			return 0.0;

		double median = median(values);
		// Environment
		// System.out.println("std: " + std(values));
		double env = kSigma * std(values);
		double lower = median - env;
		double upper = median + env;
		// create environment vector list
		Vector<Double> v = new Vector<Double>();
		for (int i = 0; i < len; i++) {
			if ((values[i] > lower) && (values[i] < upper))
				v.add(values[i]);
		}

		return median(vector2array(v));
	}

	private double[] vector2array(Vector<Double> vec) {
		double[] res = new double[vec.size()];
		for (int i = 0; i < vec.size(); i++)
			res[i] = vec.get(i);
		return res;
	}

	public double std(double values[]) {
		double res = 0.0;

		int len = values.length;
		if (len < 1)
			return 0.0;

		double u = mean(values);
		for (int i = 0; i < len; i++)
			res += (values[i] - u) * (values[i] - u);

		return Math.sqrt(res / (len - 1));
	}
}
