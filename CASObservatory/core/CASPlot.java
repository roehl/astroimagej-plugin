package CASObservatory.core;

import java.awt.Window;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.Plot;

public class CASPlot extends Plot {

	CASProfilePlot cpp;

	@SuppressWarnings("deprecation")
	public CASPlot(CASProfilePlot cpp, String title, String xLabel, String yLabel, float[] xValues, float[] yValues) {
		super(title, xLabel, yLabel, xValues, yValues);

		this.cpp = cpp;
	}

	public int getMethod() {

		ImagePlus imp = this.getImagePlus();

		if (imp != null) {
			Window win = imp.getWindow();
			if (win instanceof CASPlotWindow && win.isVisible()) {
				updateImage(); // show in existing window
				CASPlotWindow cpw = (CASPlotWindow) win;
				return cpw.getAverageMethod();
			} else
				setImagePlus(null);
		}
		CASPlotWindow cpw = new CASPlotWindow(cpp, imp, this); // note: this may

		if (IJ.isMacro() && imp != null) // wait for plot to be displayed
			IJ.selectWindow(imp.getID());
		return cpw.getAverageMethod();
	}

	@Override
	public CASPlotWindow show() {

		ImagePlus imp = this.getImagePlus();

		if (imp != null) {
			Window win = imp.getWindow();
			if (win instanceof CASPlotWindow && win.isVisible()) {
				updateImage(); // show in existing window
				CASPlotWindow cpw = (CASPlotWindow) win;
				return cpw;
			} else
				setImagePlus(null);
		}

		CASPlotWindow cpw = new CASPlotWindow(cpp, imp, this); // note: this may
																// set

		if (IJ.isMacro() && imp != null) // wait for plot to be displayed
			IJ.selectWindow(imp.getID());

		return cpw;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

}
