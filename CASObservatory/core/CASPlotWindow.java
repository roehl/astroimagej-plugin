package CASObservatory.core;

import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Point;
import java.awt.Scrollbar;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JLabel;

import CASObservatory.api.CASFile;
import ij.IJ;
import ij.ImagePlus;
import ij.gui.ImageCanvas;
import ij.gui.Plot;
import ij.gui.PlotWindow;
import ij.io.SaveDialog;
import ij.process.ImageProcessor;

public class CASPlotWindow extends PlotWindow {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	TextField ctrlTextHigh;
	int imageHigh = 20;
	static int averageMethod = 0; // Median
	CASProfilePlot cpp;
	ImagePlus myIMP;
	String originName;

	String midas_ext = ".fits";
	String ascii_ext = ".txt";
	String votable_ext = ".vot";
	String midas = "Spectrum 1D (MIDAS) *" + midas_ext;
	String ascci = "Spectrum 1D (ASCII) *" + ascii_ext;
	String votable = "Spectrum 1D (VOTABLE) *" + votable_ext;
	String strSelected = midas;
	int intSelected = 0;

	public CASPlotWindow(CASProfilePlot cpp, ImagePlus imp, CASPlot plot) {
		super(imp, plot);
		this.cpp = cpp;
		addPanel();

		// setSize(730,450);

	}

	void addPanel() {
		Panel panel = new Panel();

		/////////////
		GridBagLayout gridBagLayout = new GridBagLayout();

		gridBagLayout.columnWidths = new int[] { 100, 75, 93, 60, 93, 93, 0 };
		gridBagLayout.rowHeights = new int[] { 19, 19, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		panel.setLayout(gridBagLayout);

		JLabel ctrl1D = new JLabel("Spectrum 1D");
		GridBagConstraints gbc_ctrl1D = new GridBagConstraints();
		gbc_ctrl1D.fill = GridBagConstraints.BOTH;
		gbc_ctrl1D.insets = new Insets(0, 0, 5, 5);
		gbc_ctrl1D.gridx = 0;
		gbc_ctrl1D.gridy = 0;
		panel.add(ctrl1D, gbc_ctrl1D);

		Choice ctrlMethod = new Choice();
		ctrlMethod.add("Median");
		ctrlMethod.add("Sigma-Median");
		ctrlMethod.add("Mean");

		ctrlMethod.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				averageMethod = ctrlMethod.getSelectedIndex();
				if (averageMethod == -1)
					averageMethod = 0;

				cpp.recreate(averageMethod);

				// System.out.println(ctrlMethod.getSelectedItem());
			}
		});

		Label ctrlLabelMedthod = new Label("Method");
		GridBagConstraints gbc_ctrlLabelMedthod = new GridBagConstraints();
		gbc_ctrlLabelMedthod.anchor = GridBagConstraints.EAST;
		gbc_ctrlLabelMedthod.fill = GridBagConstraints.VERTICAL;
		gbc_ctrlLabelMedthod.insets = new Insets(0, 0, 5, 5);
		gbc_ctrlLabelMedthod.gridx = 1;
		gbc_ctrlLabelMedthod.gridy = 0;
		panel.add(ctrlLabelMedthod, gbc_ctrlLabelMedthod);
		GridBagConstraints gbc_ctrlMethod = new GridBagConstraints();
		gbc_ctrlMethod.fill = GridBagConstraints.BOTH;
		gbc_ctrlMethod.insets = new Insets(0, 0, 5, 5);
		gbc_ctrlMethod.gridx = 2;
		gbc_ctrlMethod.gridy = 0;
		panel.add(ctrlMethod, gbc_ctrlMethod);

		Label ctrlLabelFile = new Label("File:");
		GridBagConstraints gbc_ctrlLabelFile = new GridBagConstraints();
		gbc_ctrlLabelFile.anchor = GridBagConstraints.EAST;
		gbc_ctrlLabelFile.fill = GridBagConstraints.VERTICAL;
		gbc_ctrlLabelFile.insets = new Insets(0, 0, 5, 5);
		gbc_ctrlLabelFile.gridx = 3;
		gbc_ctrlLabelFile.gridy = 0;
		panel.add(ctrlLabelFile, gbc_ctrlLabelFile);

		Choice ctrlFile = new Choice();

		ctrlFile.add(midas);
		ctrlFile.add(ascci);
		// ctrlFile.add(votable);

		ctrlFile.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				strSelected = ctrlFile.getSelectedItem();
				intSelected = ctrlFile.getSelectedIndex();
				System.out.println("Choice: " + strSelected);

			}
		});
		GridBagConstraints gbc_ctrlFile = new GridBagConstraints();
		gbc_ctrlFile.fill = GridBagConstraints.BOTH;
		gbc_ctrlFile.insets = new Insets(0, 0, 5, 5);
		gbc_ctrlFile.gridx = 4;
		gbc_ctrlFile.gridy = 0;
		panel.add(ctrlFile, gbc_ctrlFile);

		Button ctrlButtonFile = new Button("Export...");
		ctrlButtonFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String ext = midas_ext;
				if (intSelected == 1)
					ext = ascii_ext;
				if (intSelected == 2)
					ext = votable_ext;
				
				originName = cpp.imp.getTitle();
				String strFileName = "spectrum_" + originName;
				// System.out.println("Filename: " + strFileName);
				SaveDialog dlg = new SaveDialog("Export: " + strSelected, strFileName, ext);

				String dir = dlg.getDirectory();
				String file = dlg.getFileName();
				if (dir != null && file != null) {
					export(dir, file, intSelected);
					// System.out.println(dir);
					// System.out.println(file);
				}

			}

		});
		GridBagConstraints gbc_ctrlButtonFile = new GridBagConstraints();
		gbc_ctrlButtonFile.anchor = GridBagConstraints.WEST;
		gbc_ctrlButtonFile.insets = new Insets(0, 0, 5, 0);
		gbc_ctrlButtonFile.gridx = 5;
		gbc_ctrlButtonFile.gridy = 0;
		panel.add(ctrlButtonFile, gbc_ctrlButtonFile);

		JLabel ctrl2D = new JLabel("Spectrum 2D");
		GridBagConstraints gbc_ctrl2D = new GridBagConstraints();
		gbc_ctrl2D.fill = GridBagConstraints.BOTH;
		gbc_ctrl2D.insets = new Insets(0, 0, 0, 5);
		gbc_ctrl2D.gridx = 0;
		gbc_ctrl2D.gridy = 1;
		panel.add(ctrl2D, gbc_ctrl2D);

		Label ctrlLabelHigh = new Label("Image High");
		GridBagConstraints gbc_ctrlLabelHigh = new GridBagConstraints();
		gbc_ctrlLabelHigh.anchor = GridBagConstraints.EAST;
		gbc_ctrlLabelHigh.insets = new Insets(0, 0, 0, 5);
		gbc_ctrlLabelHigh.gridx = 1;
		gbc_ctrlLabelHigh.gridy = 1;
		panel.add(ctrlLabelHigh, gbc_ctrlLabelHigh);

		Scrollbar ctrlScrollWidth = new Scrollbar();
		ctrlScrollWidth.addAdjustmentListener(new AdjustmentListener() {
			public void adjustmentValueChanged(AdjustmentEvent e) {

				imageHigh = ctrlScrollWidth.getValue();
				ctrlTextHigh.setText("" + imageHigh);

			}
		});
		ctrlScrollWidth.setValue(20);
		ctrlScrollWidth.setMaximum(510);
		ctrlScrollWidth.setMinimum(1);
		ctrlScrollWidth.setOrientation(Scrollbar.HORIZONTAL);
		GridBagConstraints gbc_ctrlScrollWidth = new GridBagConstraints();
		gbc_ctrlScrollWidth.fill = GridBagConstraints.BOTH;
		gbc_ctrlScrollWidth.insets = new Insets(0, 0, 0, 5);
		gbc_ctrlScrollWidth.gridx = 2;
		gbc_ctrlScrollWidth.gridy = 1;
		panel.add(ctrlScrollWidth, gbc_ctrlScrollWidth);

		ctrlTextHigh = new TextField();
		ctrlTextHigh.setText(" 20  ");
		GridBagConstraints gbc_ctrlTextHigh = new GridBagConstraints();
		gbc_ctrlTextHigh.anchor = GridBagConstraints.WEST;
		gbc_ctrlTextHigh.insets = new Insets(0, 0, 0, 5);
		gbc_ctrlTextHigh.gridx = 3;
		gbc_ctrlTextHigh.gridy = 1;
		panel.add(ctrlTextHigh, gbc_ctrlTextHigh);

		Checkbox ctrlCheckboxShow = new Checkbox("Snapshot Image...");
		ctrlCheckboxShow.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				boolean checked = ctrlCheckboxShow.getState();
				if (checked)
					createImage();
				else
					closeImage();
			}
		});

		GridBagConstraints gbc_ctrlCheckboxShow = new GridBagConstraints();
		gbc_ctrlCheckboxShow.fill = GridBagConstraints.BOTH;
		gbc_ctrlCheckboxShow.insets = new Insets(0, 0, 0, 5);
		gbc_ctrlCheckboxShow.gridx = 4;
		gbc_ctrlCheckboxShow.gridy = 1;
		panel.add(ctrlCheckboxShow, gbc_ctrlCheckboxShow);
		///////////

		/*
		 * 
		 */
		add(panel);
		pack();
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		Point loc = getLocation();
		Dimension size = getSize();
		if (loc.y + size.height > screen.height)
			getCanvas().zoomOut(0, 0);
	}

	public boolean createImage() {

		String title = "2D Spectrum Image - from " + cpp.imp.getTitle();

		int imageWidth = cpp.getProfileWidth();
		double profile[] = cpp.getProfile();
		myIMP = IJ.createImage(title, "GRAY32", imageWidth, imageHigh, 1);
		ImageProcessor ip = myIMP.getProcessor();

		for (int i = 0; i < imageWidth; i++) {
			for (int j = 0; j < imageHigh; j++) {
				ip.putPixelValue(i, j, profile[i]);
			}
		}

		myIMP.show();
		IJ.selectWindow(title);
		IJ.run("Enhance Contrast", "saturated=0.5");
		return true;
	}

	public void closeImage() {
		myIMP.close();
	}

	public static int getAverageMethod() {
		return averageMethod;
	}

	private void export(String dir, String file, int intSelected) {
		CASFile casFile = new CASFile();
		switch (intSelected) {
		case 0: { // midas
			casFile.exportMidas(dir, file, cpp.getProfile(), cpp.getProfileRect(), originName,averageMethod);
			break;
		}
		case 1: { // ascii
			String sep = ",";
			String header = "# pixel" + sep + "GrayValue";
			casFile.exportASCII(dir, file, cpp.getProfile(), ",", header);
			break;
		}
		case 2: { // votable
			casFile.exportVOTABLE(dir, file);
			break;
		}
		}

	}

}
