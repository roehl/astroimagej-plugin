# AstroImageJ in der Amateurastronomie (PlugIn-Development)

Die Installation erfordert nur das herunterladen und kopieren in das ImageJ-plugins Verzeichnis, und ist damit recht einfach.

Die Plugins werden wenn es geht für ImageJ entwickelt, da AstroImageJ gegenüber ImageJ immer veraltet ist.
    
    
### Install the Plugin in ImageJ

The installation is a download and copy process.

* Download the repository and unzip it on your pc:

    * Go to: https://bitbucket.org/roehl/astroimagej-plugin/src/master/CASObservatory/CASObervatory 
    * Within bitbucket find the '...'-Button (to the right). Use the 'Download repository' link
        * Unpack the zip-file into a directory of your choice.
  
        
* Go to your ImageJ install directory 
     * 'cd' into the plugins directory
     * copy all files (e.g. fits.jar) in the CASObervatory thirdparty directory in the plugins directory
        * These jar-files are necessary to run the CASObservatory-Plugnis properly.
     * copy the directory CASObservatory in the plugins directory
  

### ImageJ directory structure

    imagej (install directory)
    │   ij.jar                  # main application file
    │   ...                     # other files
    │    
    └───macros
    │
    └───luts
    │
    └───jre
    │
    └───images
    │   
    └───plugins                 # the central folder for plugins
        │   fits.jar            # CASObservatory (thirdparty) jar-files
        │   ...                 # other files
        └───CASObservatory      # Unpacked directory from bitbucket
        │   │   ...
        │
        └───other directory
        │   │   ...
        │...
        
                                 