# This file is useful only for developers :-)
# 
# -toolchain bitbucket eclipse imagej plugin development
# -the central directory within imagej is the plugins directory
# -a working eclipse imagej environment should be setup before
#  using the recipies given  here
#

#
# local-repro of the developer is: /devel/projects/astronomy/ImageJ03
#

[bitbucket with eclipse - imagej]
#
# first initialisation with eclipse/imagej and bitbucket
#

step 1: plugins ordner umbennnen plugins_XXX
step 2: git clone https://roehl@bitbucket.org/roehl/astroimagej-plugin.git plugins
        git in den plugins ordner clonen (plugins wird neu erstellt)
step 3: inhalt von plugins_XXX in das neu erstellte (geklonte) verzeichnis kopieren
step 4: git add CASObservatory
step 5: git commit -m "init text"
step 6: git origin master


[git usage basics] 
# for the absolute beginners
# working on console -> go to the working directory (CASObservatory)


### workflow 01: add new file or add changes on existing file
git add filename
git commit -m "info-text"
git origin master

[Install Plugin in ImageJ]
#
# currently all external jars must be copied in the image plugin folder too
#
step 1: copy CASObervatory Directory into ImageJ plugin directory
step 2: copy thirdparty jar (fits.jar) into plugin directory
 
